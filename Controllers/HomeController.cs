﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace SampleMvcApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;

        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public async Task<IActionResult> Index()
        {
            var account = getAccount();

            if (account != null)
            {
                var response = await makeRequest();

                return View("Index", await response.Content.ReadAsStringAsync());
            }

            return View("Index", "niet ingelogd.");
        }

        private string getAccount()
        {
            var accountClaim = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.bertus.com/baspas/2013/10/account");

            return accountClaim?.Value;
        }

        private async Task<HttpResponseMessage> makeRequest()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", _configuration["APIManager:Ocp_Apim_Subscription_Key"]);
            
            var account = getAccount();

            string idToken = await HttpContext.GetTokenAsync("id_token");

            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {idToken}");

            var uri = $"https://mybertus-api.bertus.com/integration/api/v1/accounts/{account}/shippingAddresses?" + queryString;

            var response = await client.GetAsync(uri);

            return response;
        }

        /*
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "{subscription key}");
            client.DefaultRequestHeaders.Add("Authorization", "{id token}");

            var uri = "https://mybertus-api.bertus.com/integration/api/v1/accounts/{accountId}/shippingAddresses?" + queryString;

            var response = await client.GetAsync(uri);
        */

        public IActionResult Error()
        {
            return View();
        }
    }
}
